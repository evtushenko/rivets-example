/* global require */
/* global __dirname */
var gulp    = require('gulp');
var jst     = require('gulp-jst-concat');
var amdwrap = require('gulp-wrap-amd');
var rjs     = require('gulp-requirejs');
var amdclean= require('gulp-amdclean');
var uglify  = require('gulp-uglify');
var sass    = require('gulp-ruby-sass');
var connect = require('gulp-connect');

gulp.task('sass', function() {
    return sass([
        __dirname + '/assets/src/sass/ios-switcher.scss'
    ])
        .pipe(gulp.dest(__dirname + '/assets/dist/css'));
});
gulp.task('jst', function() {
    return gulp.src(__dirname + '/assets/src/js/templates/**/*.html')
        .pipe(jst('templates.js', {
            renameKeys: ['^.*templates/(.*).html$', '$1']
        }))
        .pipe(amdwrap({
            deps:       [],
            params:     [],
            exports:    'JST',
            moduleRoot: ''
        }))
        .pipe(gulp.dest(__dirname + '/assets/src/js'));
});
gulp.task('rjs', ['jst'], function() {
    rjs({
        baseUrl:    __dirname + '/assets/src/js',
        out:        'app.js',
        include:    ['bootstrap']
    })
        .pipe(amdclean.gulp({
            prefixTransform: function(name) {
                return 'module_' + name;
            }
        }))
        .pipe(uglify({
            mangle:     true,
            compress:   {}
        }))
        .pipe(gulp.dest(__dirname + '/assets/dist/js'));
});

gulp.task('watch', function() {
    gulp.watch([
        __dirname + '/assets/src/js/templates/**/*.html'
    ], [
        'rjs'
    ]);
    gulp.watch([
        __dirname + '/assets/src/js/**/*.js',
        '!'+__dirname + '/assets/src/js/templates.js'
    ], [
        'rjs'
    ]);
});

gulp.task('server', ['watch'], function() {
    connect.server({
        root: __dirname,
        port: 9000
    });
});

gulp.task('build', ['sass', 'rjs']);
gulp.task('default', ['build']);