define([
    'app',
    'templates',
    'views/form/base'
], function(app, templates, BaseClass) {
    return BaseClass.extend({
        tagName:    'div',
        template:   templates['form/vue'],

        initialize: function() {
            this.state = {
                is_valid:   this.model.isValid(),
                errors:     {}
            };

            this.listenTo(this.model, 'validated', function(isValid, model, errors) {
                this.state.is_valid = isValid;
                this.state.errors = errors;
            });
            this.listenTo(this.model, 'change', function() {
                this.model.validate();
            });

            _.bindAll(this, 'reset', 'submit');
        },
        onRender: function() {
            this.binder = new Vue({
                el:     this.el,
                data:   {
                    view:       this,
                    model:      this.model.attributes,
                    state:      this.state,

                    tags:       app.tags,
                    types:      app.types,
                    categories: app.categories,

                    ms_config:  this.multiselectConfig
                }
            });
        },
        onBeforeDestroy: function() {
            if (this.binder) {
                this.binder.$remove();
            }
        },
        serializeData: function() {
            return {
                multiselectConfig: this.multiselectConfig
            };
        }
    });
});