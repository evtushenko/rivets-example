define([
    'app',
    'templates',
    'views/form/base',
    'views/components/ios-switcher',
    'mixins/errorable'
], function(app, templates, BaseClass, SwitcherView, ErrorableMixin) {
    var View = BaseClass.extend({
        tagName:    'div',
        template:   templates['form/stickit'],
        events:     {
            'click @ui.btnSubmit':  'submit',
            'click @ui.btnReset':   'reset'
        },
        regions:    {
            switcher: '[data-role="switcher"]'
        },
        ui:         {
            btnSubmit:  '[data-role="btn-submit"]',
            btnReset:   '[data-role="btn-reset"]'
        },
        bindings:   {
            '[data-attr="name"]':           'name',
            '[data-attr="type"]':           'type',
            '[data-attr="tags"]':           'tags',
            '[data-attr="has_tags"]':       'has_tags',
            '[data-attr="count"]':          {
                observe: 'count',
                onSet: function(v) {
                    return isNaN(v = parseInt(v)) ? null : v;
                },
                onGet: function(v) {
                    return !_.isNumber(v) ? '' : v.toString();
                }
            },
            '[data-attr="category_id"]':    {
                observe: 'category_id',
                onSet: function(v) {
                    return isNaN(v = parseInt(v)) ? null : v;
                },
                onGet: function(v) {
                    return !_.isNumber(v) ? '' : v.toString();
                }
            },

            '[data-role="tags-form"]':      {
                observe: 'has_tags',
                visible: true
            },
            '[data-role="model-attrs"]':    {
                observe: ['name', 'type', 'tags', 'has_tags', 'count', 'category_id', 'is_enabled'],
                onGet: function() {
                    return JSON.stringify(this.model.toJSON(), null, 4);
                }
            },
            '[data-role="view-desc"]':      {
                observe: ['name', 'type', 'tags', 'has_tags', 'count', 'category_id', 'is_enabled'],
                updateMethod: 'html',
                onGet: function() {
                    return this.getDescription();
                }
            },
            '[data-role="random-color"]':   {
                observe: ['color'],
                update: function($el, v) {
                    $el.css('background-color', v);
                }
            },
            '[data-role="collapse-block"]': {
                observe: 'is_enabled',
                update: function($el, v) {
                    if (!$el.hasClass('collapse')) {
                        $el.addClass(v ? 'collapse in' : 'collapse');
                    } else {
                        $el.collapse(v ? 'show' : 'hide');
                    }
                }
            }
        },
        errorAttrs: {
            name:           '[data-attr="name"]',
            type:           '[data-attr="type-list"]',
            tags:           '[data-attr="tags"]',
            count:          '[data-attr="count"]',
            category_id:    '[data-attr="category_id"]'
        },

        onRender: function() {
            this.stickit();
            this.listenButtonState();
            this.listenValidationState();

            this.switcher.show(new SwitcherView({
                model:      this.model,
                attribute:  'is_enabled',
                duration:   300
            }));

            this.$lists = this.$('select[multiple]');
            this.$lists.multiselect(this.multiselectConfig);
            this.listenTo(this.model, 'change:tags', function() {
                this.$lists.multiselect('refresh');
            });
        },
        onBeforeDestroy: function() {
            this.$lists.multiselect('destroy');

            this.unstickit();
        },
        serializeData: function() {
            return {
                model:      this.model.toJSON(),
                categories: app.categories,
                types:      app.types,
                tags:       app.tags
            };
        },
        listenButtonState: function()
        {
            this.listenTo(this.model, 'validated', function(isValid) {
                this.ui.btnSubmit.attr('disabled', !isValid);
            });

            this.ui.btnSubmit.attr('disabled', !this.model.isValid());
        },
        listenValidationState: function() {
            this.listenTo(this.model, 'change', function() {
                this.model.validate();
            });
            this.listenTo(this.model, 'validated:invalid', function(model, errors) {
                this.toggleErrors(errors);
            });
            this.listenTo(this.model, 'validated:valid', function() {
                this.hideErrors();
            });
        }
    });
    _.extend(View.prototype, ErrorableMixin);

    return View;
});