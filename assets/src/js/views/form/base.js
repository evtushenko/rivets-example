define([

], function() {
    return Backbone.Marionette.LayoutView.extend({
        tagName:    'div',
        attributes: {
            style: 'margin-bottom: 100px'
        },
        multiselectConfig: {
            numberDisplayed:    10,
            buttonClass:        'form-control multiline',
            buttonWidth:        '100%',
            buttonContainer:    '<div />',
            maxHeight:          300
        },
        getDescription: function() {
            return [
                '<b>Name:</b> ' + this.model.get('name'),
                '<b>Count:</b> ' + this.model.get('count'),
                '<b>Tags:</b> ' + this.model.get('tags').join(', ')
            ].join('; ');
        },
        submit: function(e) {
            e && e.preventDefault();

            alert(JSON.stringify(this.model.attributes, null, 4));
        },
        reset: function(e) {
            e && e.preventDefault();

            this.model.set(this.model.defaults);
        }
    });

});