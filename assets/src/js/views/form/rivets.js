define([
    'app',
    'templates',
    'views/form/base'
], function(app, templates, BaseClass) {
    return BaseClass.extend({
        tagName:    'div',
        template:   templates['form/rivets'],

        initialize: function() {
            this.state = {
                is_valid:   this.model.isValid(),
                errors:     {}
            };

            this.listenTo(this.model, 'validated', function(isValid, model, errors) {
                this.state.is_valid = isValid;
                this.state.errors = errors;
            });
            this.listenTo(this.model, 'change', function() {
                this.model.validate();
            });

            _.bindAll(this, 'reset', 'submit');
        },
        onRender: function() {
            this.binder = rivets.bind(this.$el, {
                view:       this,
                model:      this.model,
                state:      this.state,

                tags:       app.tags,
                types:      app.types,
                categories: app.categories
            });
        },
        onBeforeDestroy: function() {
            if (this.binder) {
                this.binder.unbind();
            }
        },
        serializeData: function() {
            return {
                multiselectConfig: this.multiselectConfig
            };
        }
    });
});