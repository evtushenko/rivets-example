define([
    'app',
    'templates',
    'views/form/base',
    'ractive/decorators/multiselect',
    'ractive/decorators/tooltip',
    'ractive/components/ios-switcher'
], function(app, templates, BaseClass, MultiselectDecorator, TooltipDecorator, IosSwitcherComponent) {
    return BaseClass.extend({
        tagName:    'div',
        template:   false,

        initialize: function() {
            this.state = {
                is_valid:   this.model.isValid(),
                errors:     {}
            };

            this.listenTo(this.model, 'validated', function(isValid, model, errors) {
                this.state.is_valid = isValid;
                this.state.errors = _.extend(this.state.errors, errors);

                if (this.ractive) {
                    this.ractive.set('state.is_valid', isValid);
                    this.ractive.set('state.errors', errors);
                }
            });
            this.listenTo(this.model, 'change', function() {
                this.model.validate();
            });
        },
        _renderTemplate: function() {
            this.initRactive();
            BaseClass.prototype._renderTemplate.call(this);
        },
        initRactive: function() {
            this.ractive = new Ractive({
                el:         this.el,
                template:   templates['form/ractive'],
                adapt:      ['Backbone'],
                decorators: {
                    multiselect:    MultiselectDecorator,
                    tooltip:        TooltipDecorator
                },
                components: {
                    'ios-switcher': IosSwitcherComponent
                },
                data:       {
                    view:   this,
                    state:  this.state,
                    model:  this.model,
                    config: {
                        multiselect: this.multiselectConfig,
                        tooltip: {placement: 'top'}
                    },
                    lists:  {
                        tags:       app.tags,
                        types:      app.types,
                        categories: app.categories
                    },
                    json: function() {
                        return JSON.stringify(this.get('model').toJSON(), null, 4);
                    },
                    description: function() {
                        return this.get('view').getDescription();
                    }
                }
            });

            this.ractive.on('onReset', _.bind(function(event) {
                this.reset(event.original);
            }, this));
            this.ractive.on('onSubmit', _.bind(function(event) {
                this.submit(event.original);
            }, this));
            this.ractive.on('onToggle', _.bind(function() {
                this.model.set('is_enabled', !this.model.get('is_enabled'));
            }, this));
        }
    });
});