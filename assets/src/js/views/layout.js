define([
    'app',
    'templates',
    'models/form',
    'views/form/rivets',
    'views/form/stickit',
    'views/form/ractive',
    'views/form/vue'
], function(app, templates, FormModel, RivetsFormView, StickitFormView, RactiveFormView, VueFormView) {
    return Backbone.Marionette.LayoutView.extend({
        tagName:    'div',
        template:   templates['layout'],
        events:     {
            'click @ui.btnRender':  'renderAll',
            'click @ui.btnUpdate':  'updateAll',
            'click @ui.btnRemove':  'removeAll'
        },
        ui: {
            btnRender:  '[data-role="btn-render"]',
            btnUpdate:  '[data-role="btn-update"]',
            btnRemove:  '[data-role="btn-remove"]',
            itemCount:  '[data-role="item-count"]'
        },
        regions:    {
            rivets:     '[data-role="region-rivets"]',
            stickit:    '[data-role="region-stickit"]',
            ractive:    '[data-role="region-ractive"]',
            vue:        '[data-role="region-vue"]'
        },
        initialize: function() {
            _.bindAll(this, 'renderAll', 'updateAll');

            this.collectionRivets   = new Backbone.Collection([]);
            this.collectionStickit  = new Backbone.Collection([]);
            this.collectionRactive  = new Backbone.Collection([]);
            this.collectionVue      = new Backbone.Collection([]);
        },
        onRender: function() {
            this.rivets.show(new Backbone.Marionette.CollectionView({
                childView:  RivetsFormView,
                collection: this.collectionRivets
            }));

            this.stickit.show(new Backbone.Marionette.CollectionView({
                childView:  StickitFormView,
                collection: this.collectionStickit
            }));

            this.ractive.show(new Backbone.Marionette.CollectionView({
                childView:  RactiveFormView,
                collection: this.collectionRactive
            }));

            this.vue.show(new Backbone.Marionette.CollectionView({
                childView:  VueFormView,
                collection: this.collectionVue
            }));

            this.renderAll();
        },

        renderAll: function(e) {
            e && e.preventDefault();

            this.toggleButtons(false);

            this.is_default_state = true;
            app.seedData();

            var modelsRivets = this.createModels();
            var modelsStickit = this.createModels();
            var modelsRactive = this.createModels();
            var modelsVue = this.createModels();

            this.collectionRivets.reset([]);
            this.collectionStickit.reset([]);
            this.collectionRactive.reset([]);

            $.when(
                this.doDeferred(function() {
                    console.time('render rivets.js');
                    this.collectionRivets.reset(modelsRivets);
                    console.timeEnd('render rivets.js');
                }),
                this.doDeferred(function() {
                    console.time('render stick.it');
                    this.collectionStickit.reset(modelsStickit);
                    console.timeEnd('render stick.it');
                }),
                this.doDeferred(function() {
                    console.time('render ractive.js');
                    this.collectionRactive.reset(modelsRactive);
                    console.timeEnd('render ractive.js');
                }),
                this.doDeferred(function() {
                    console.time('render vue.js');
                    this.collectionVue.reset(modelsVue);
                    console.timeEnd('render vue.js');
                })
            ).then(_.bind(function() {
                    this.toggleButtons(true);
                }, this));
        },
        removeAll: function(e) {
            e && e.preventDefault();

            this.toggleButtons(false);

            $.when(
                this.doDeferred(function() {
                    console.time('remove rivets.js');
                    this.collectionRivets.reset([]);
                    console.timeEnd('remove rivets.js');
                }),
                this.doDeferred(function() {
                    console.time('remove stick.it');
                    this.collectionStickit.reset([]);
                    console.timeEnd('remove stick.it');
                }),
                this.doDeferred(function() {
                    console.time('remove ractive.js');
                    this.collectionRactive.reset([]);
                    console.timeEnd('remove ractive.js');
                }),
                this.doDeferred(function() {
                    console.time('remove vue.js');
                    this.collectionVue.reset([]);
                    console.timeEnd('remove vue.js');
                })
            ).then(_.bind(function() {
                    this.toggleButtons(true);
                }, this));
        },
        updateAll: function(e) {
            e && e.preventDefault();

            this.toggleButtons(false);

            var attrs;
            if (this.is_default_state) {
                attrs = this.getRandomAttributes();
                this.is_default_state = false;
            } else {
                attrs = this.getDefaultAttributes();
                this.is_default_state = true;
            }

            $.when(
                this.doDeferred(function() {
                    console.time('update rivets.js');
                    this.collectionRivets.each(function(model) {
                        model.set(attrs);
                    });
                    console.timeEnd('update rivets.js');
                }),
                this.doDeferred(function() {
                    console.time('update stick.it');
                    this.collectionStickit.each(function (model) {
                        model.set(attrs);
                    });
                    console.timeEnd('update stick.it');
                }),
                this.doDeferred(function() {
                    console.time('update ractive.it');
                    this.collectionRactive.each(function (model) {
                        model.set(attrs);
                    });
                    console.timeEnd('update ractive.it');
                }),
                this.doDeferred(function() {
                    console.time('update vue.it');
                    this.collectionVue.each(function (model) {
                        model.set(attrs);
                    });
                    console.timeEnd('update vue.it');
                })
            ).then(_.bind(function() {
                    this.toggleButtons(true);
                }, this));
        },
        doDeferred: function(func) {
            var defer = $.Deferred();
            _.defer(_.bind(function() {
                func.call(this);
                defer.resolve();
            }, this));

            return defer.promise();
        },
        toggleButtons: function(enable) {
            this.ui.btnRender.attr('disabled', !enable);
            this.ui.btnUpdate.attr('disabled', !enable);
            this.ui.btnRemove.attr('disabled', !enable);
        },
        getRandomAttributes: function() {
            return {
                name:           faker.company.companyName(),
                type:           faker.random.array_element(app.types),
                count:          parseInt(Math.random() * 1000),
                color:          faker.internet.color(),
                category_id:    faker.random.array_element(app.categories).id,
                has_tags:       true,
                is_enabled:     true,
                tags:           [
                    faker.random.array_element(app.tags),
                    faker.random.array_element(app.tags),
                    faker.random.array_element(app.tags),
                    faker.random.array_element(app.tags)
                ]
            };
        },
        getDefaultAttributes: function() {
            return FormModel.prototype.defaults;
        },
        createModels: function() {
            var itemCount = parseInt(this.ui.itemCount.val());
            return _.map(_.range(0, itemCount), function(i) {
                return new FormModel({id: i + 1});
            });
        }
    });
});