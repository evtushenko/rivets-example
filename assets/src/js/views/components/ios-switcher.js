define([
    'templates'
], function(templates) {
    return Backbone.Marionette.ItemView.extend({
        tagName:    'span',
        template:   templates['components/ios-switcher'],
        events:     {
            'click @ui.frame': 'onToggle'
        },
        ui:         {
            frame:  '[data-role="switcher-frame"]',
            button: '[data-role="switcher-button"]'
        },

        initialize: function() {
            this.duration = parseInt(this.options.duration) || 250;
            this.attribute = this.options.attribute;
        },
        onRender: function() {
            this.listenStatus();
            this.setTransitionDuration();
        },
        listenStatus: function() {
            this.model.get(this.attribute) ? this.setOn() : this.setOff();
            this.listenTo(this.model, 'change:' + this.attribute, function() {
                this.model.get(this.attribute) ? this.setOn() : this.setOff();
            });
        },
        setTransitionDuration: function() {
            var duration = (this.duration / 1000).toFixed(2) + 's';

            this.ui.frame.css('transition-duration', duration);
            this.ui.button.css('transition-duration', duration);
        },
        onToggle: function() {
            if (this.isDisabled()) {
                return;
            }

            this.model.set(this.attribute, !this.model.get(this.attribute));
            this.disableClick();
        },

        setOn: function() {
            this.ui.frame.addClass('on');
            this.trigger('switcher:on');
        },
        setOff: function() {
            this.ui.frame.removeClass('on');
            this.trigger('switcher:off');
        },

        isDisabled: function() {
            return this.is_busy;
        },
        disableClick: function() {
            this.is_busy = true;
            _.delay(_.bind(function() {
                this.is_busy = false;
            }, this), this.duration);
        }
    });
});