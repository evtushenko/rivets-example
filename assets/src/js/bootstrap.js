define([
    'app',
    'views/layout'
], function(app, LayoutView) {
    app.addInitializer(function() {
        app.container.show(new LayoutView());
    });
    app.start();
});