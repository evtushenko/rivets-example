define([
    'app'
], function(app) {
    var Model = Backbone.Model.extend({
        defaults: {
            id:             null,
            name:           '',
            type:           null,
            count:          null,
            category_id:    null,
            has_tags:       false,
            tags:           [],
            color:          '#dddddd',
            is_enabled:     false
        },
        validation: {
            name: function(v) {
                if ($.trim(v) === '') {
                    return 'Name is required';
                } else if (v.length > 100) {
                    return 'Max length is 100 chars';
                }

                return undefined;
            },
            category_id: function(v) {
                var exists = _.find(app.categories, function(item) {
                    return v === item.id;
                });
                if (!exists) {
                    return 'Select category';
                }

                return undefined;
            },
            type: function(v) {
                if (_.indexOf(app.types, v) < 0) {
                    return 'Select type';
                }

                return undefined;
            },
            count: function(v) {
                if (!_.isNumber(v)) {
                    return 'Type number';
                }

                return undefined;
            },
            tags: function(v) {
                if (this.get('has_tags') && !v.length) {
                    return 'Select tags';
                }

                return undefined;
            }
        }
    });
    _.extend(Model.prototype, Backbone.Validation.mixin);

    return Model;
});