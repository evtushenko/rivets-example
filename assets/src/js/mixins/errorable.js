define([
], function() {
    return {
        showError: function(attr, msg) {
            if (!_.has(this.errorAttrs, attr)) {
                return;
            }

            this.$(this.errorAttrs[attr])
                    .tooltip({placement: 'top', trigger: 'manual', title: msg, animation: false})
                    .tooltip('show');
        },
        hideError: function(attr) {
            if (!_.has(this.errorAttrs, attr)) {
                return;
            }

            this.$(this.errorAttrs[attr]).tooltip('destroy');
        },
        showErrors: function(errors) {
            _.each(errors, function(msg, attr) {
                this.showError(attr, msg);
            }, this);
        },
        hideErrors: function() {
            _.each(this.errorAttrs, function(selector, attr) {
                this.hideError(attr);
            }, this);
        },
        toggleErrors: function(errors) {
            _.each(errors, function(msg, attr) {
                this.showError(attr, msg);
            }, this);
            _.each(this.errorAttrs, function(selector, attr) {
                if (!_.has(errors, attr)) {
                    this.hideError(attr);
                }
            }, this);
        }
    };
});