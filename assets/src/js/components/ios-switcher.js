define([
    'views/components/ios-switcher'
], function(SwitcherView) {
    return {
        static:     ['attribute', 'duration'],
        template:   function() {
            return '';
        },
        initialize: function(el, data) {
            (new SwitcherView({
                el:         el,
                attribute:  data.attribute,
                duration:   data.duration,
                model:      data.model
            })).render();
        }
    };
});