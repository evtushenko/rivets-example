define([

], function () {
    'use strict';

    return Ractive.extend({
        template: [
            '<span on-click="toggle" class="ios-switcher {{ enabled ? \'on\' : \'\' }}">',
            '<span class="ios-switcher-btn"></span>',
            '</span>'
        ].join('')
    });
});