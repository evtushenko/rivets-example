define([

], function () {
    'use strict';

    return function (node, options) {
        var ractive = node._ractive.root;
        var setting = false;
        var observer;

        if (!options) {
            options = {};
        }

        // Push changes from ractive to chosen
        if (node._ractive.binding) {
            observer = ractive.observe(node._ractive.binding.keypath, function(newvalue, oldvalue) {
                if (setting) {
                    return;
                }

                setting = true;
                _.defer(function () {
                    if (newvalue === '' || newvalue !== oldvalue) {
                        $(node).multiselect('refresh');
                    }

                    $(node).change();
                    setting = false;
                });
            });
        }

        // Pull changes from chosen to ractive
        $(node).multiselect(options).on('change', function() {
            if (setting) {
                return;
            }

            setting = false;
            ractive.updateModel();
        });

        return {
            teardown: function() {
                $(node).multiselect('destroy');

                if (observer) {
                    observer.cancel();
                }
            }
        };
    };
});