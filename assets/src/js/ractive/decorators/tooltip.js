define([

], function () {
    'use strict';

    return function (node, message, options) {
        if (message) {
            $(node).tooltip(_.extend({}, options, {
                trigger: 'manual',
                title: message
            })).tooltip('show');
        } else {
            $(node).tooltip('destroy');
        }

        return {
            teardown: function() {
                $(node).tooltip('destroy');
            }
        };
    };
});