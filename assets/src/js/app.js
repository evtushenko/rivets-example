define([
    'components/ios-switcher'
], function(IosSwitcher) {
    rivets.configure({
        prefix:             'data-rv',
        preloadData:        true,
        rootInterface:      '.',
        templateDelimiters: ['{{', '}}']
    });
    rivets.formatters.concat = function(l, r) {
        return (l === null || l === undefined || !l.toString ? '' : l.toString()) +
            (r === null || r === undefined || !r.toString ? '' : r.toString());
    };
    rivets.formatters.intval = {
        read: function(v) {
            return !_.isNumber(v) ? '' : v.toString();
        },
        publish: function(v) {
            return isNaN(v = parseInt(v)) ? null : v;
        }
    };
    rivets.formatters.json = function(v) {
        return JSON.stringify(v, null, 4);
    };
    rivets.binders.color = function(el, v) {
        return el.style.backgroundColor = v;
    };
    rivets.binders.collapse = function(el, v) {
        var $el = $(el);
        if (!$el.hasClass('collapse')) {
            $el.addClass(v ? 'collapse in' : 'collapse');
        } else {
            $el.collapse(v ? 'show' : 'hide');
        }
    };
    rivets.binders.tooltip = {
        bind: function(el) {
            var $el = $(el);
            var place = $el.data('tooltipPlace') || 'top';

            this.toggleTooltip = function(message) {
                if (message) {
                    $el.tooltip({
                        placement:  place,
                        trigger:    'manual',
                        title:      message
                    }).tooltip('show');
                } else {
                    $el.tooltip('destroy');
                }
            };
        },
        routine: function(el, message) {
            this.toggleTooltip(message);
        }
    };
    rivets.binders.multiselect = {
        unbind: function(el) {
            if (this.__ms_binded) {
                $(el).multiselect('destroy');
            }
        },
        routine: function(el) {
            if (this.__ms_binded) {
                $(el).multiselect('refresh');
                return;
            }

            this.__ms_binded = true;

            var config = $(el).data('multiselectConfig');
            if (!_.isObject(config)) {
                config = {};
            }

            $(el).multiselect(config);
        }
    };
    rivets.components['ios-switcher'] = IosSwitcher;


    Vue.filter('intval', {
        read: function(v) {
            return !_.isNumber(v) ? '' : v.toString();
        },
        write: function(v) {
            return isNaN(v = parseInt(v)) ? null : v;
        }
    });
    Vue.directive('multiselect', {
        priority: -10000,
        bind: function () {
            this.__ms_binded = false;
            this.__ms_config = $(this.el).data('multiselectConfig');
            if (!_.isObject(this.__ms_config)) {
                this.__ms_config = {};
            }

            _.defer(_.bind(function() {
                if (this.__ms_binded) {
                    $(this.el).multiselect('rebuild');
                } else {
                    $(this.el).multiselect(this.__ms_config);
                    this.__ms_binded = true;
                }
            }, this));
        },
        update: function () {
            if (this.__ms_binded) {
                $(this.el).multiselect('refresh');
            } else {
                $(this.el).multiselect(this.__ms_config);
                this.__ms_binded = true;
            }
        },
        unbind: function () {
            if (this.__ms_binded) {
                $(this.el).multiselect('destroy');
            }
        }
    });
    Vue.directive('collapse', {
        update: function (v) {
            var $el = $(this.el);
            if (!$el.hasClass('collapse')) {
                $el.addClass(v ? 'collapse in' : 'collapse');
            } else {
                $el.collapse(v ? 'show' : 'hide');
            }
        }
    });
    Vue.component('ios-switcher', {
        template: [
            '<span v-on="click: toggle" class="ios-switcher {{ enabled ? \'on\' : \'\' }}">',
            '<span class="ios-switcher-btn"></span>',
            '</span>'
        ].join(''),
        replace: true,
        data: {
            enabled: false
        },
        methods: {
            toggle: function () {
                this.enabled = !this.enabled;
            }
        }
    });

    var app = new Backbone.Marionette.Application();
    app.addRegions({
        container: '[data-role="container"]'
    });

    app.categories = app.tags = app.types = [];
    app.seedData = function() {
        app.categories = _.map(_.range(0, 100), function(i) {
            return {id: i + 1, name: faker.company.companyName()};
        });
        app.tags = _.map(_.range(0, 20), function() {
            return faker.lorem.words(1)[0];
        });
        app.types = _.map(_.range(0, 5), function() {
            return faker.address.country(1);
        });
    };

    return app;
});